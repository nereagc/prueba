class Cuadrado: 
    """Un ejemplo de una clase para los Cuadrados"""
    def __init__(self,l=1): 
        self.lado= l
        self.miarea = self.lado**2
    def perimetro(self):
        return self.lado*2
    def area(self):
        return self.lado**2

cuadrado01 = Cuadrado(2)
cuadrado02 = Cuadrado()
#print(perimetro(cuadrado01))
print(cuadrado01.perimetro())

    